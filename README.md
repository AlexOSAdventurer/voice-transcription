There are two relevant Jupyter notebooks here:
	1. "Gender Determination.ipynb" - this notebook demonstrates training models that determine the gender of a given voice.
	2. "Transcription.ipynb" - this notebook shows current efforts to transcribe the text of a given voice recording.

